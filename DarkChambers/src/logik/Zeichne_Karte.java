package logik;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import model.Karte;


public class Zeichne_Karte {

	private Karte karte;

	public Zeichne_Karte(Karte karte) {
		this.karte = karte;

	}

	public void render(GameContainer gc, Graphics g) throws SlickException {
//		g.drawImage(karte.getHintergrund(), 0, 0);
		
		g.setColor(Color.white);

		for (int i = 0; i < karte.getMauerteile().size(); i++) {
			g.fillRect(karte.getMauerteile().get(i).getX1(), karte.getMauerteile().get(i).getY1(),
					karte.getMauerteile().get(i).getBreite(), karte.getMauerteile().get(i).getHoehe());
			
			
		}

		// g.drawRect((float) sp1.getXpos(), (float) sp1.getYpos(), 10, 10);
	}

}

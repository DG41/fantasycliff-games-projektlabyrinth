package logik;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.Animation;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

import model.Karte;
import model.Mauer;
import model.Spieler;

public class DarkChambers extends BasicGame {
	public Spieler sp1;
	private Rectangle r;
	private Rectangle sp1r;

	private Zeichne_Karte zk;
	private Zeichne_Gegner zg;

	private int spielergroesseX = 160;
	private int spielergroesseY = 160;
	private static int fensterX = 1000;
	private static int fensterY = 800;

	public DarkChambers(String gamename) {
		super(gamename);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		sp1 = new Spieler(1.0, 1.0, 1, 1, 1, false, 1, 1,
				new Animation(new SpriteSheet("/bilder/testmodel.png", 32, 32), 200));
		r = new Rectangle(420, 420, 100, 100);
		sp1r = new Rectangle((float) sp1.getXpos(), (float) sp1.getYpos(), spielergroesseX, spielergroesseY);

		// hier m�ssen mauern und karten (... gegner, etc.) definiert werden

		Karte karte = new Karte(new Image("bilder/background.jpg"));
		karte.getMauerteile().add(new Mauer(10, 100, 20, 120));
		zk = new Zeichne_Karte(karte);
		// zg = new Zeichne_Gegner(gegner);

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		Input input = gc.getInput();
//durchlesen
		if (input.isKeyDown(Input.KEY_W) || input.isKeyDown(Input.KEY_UP)) {
			r.setY(r.getY() + (1 * delta));

			if (r.intersects(sp1r) == true) {
			} else {
				sp1.setYpos(sp1.getYpos() - (1 * delta));
				sp1r.setY((float) sp1.getYpos());
				if (sp1.getYpos() < 0) {
					sp1.setYpos(0);
					sp1r.setY(0);
				}
			}

			r.setY(r.getY() - (1 * delta));
		}

		if (input.isKeyDown(Input.KEY_A) || input.isKeyDown(Input.KEY_LEFT)) {
			r.setX(r.getX() + (1 * delta));

			if (r.intersects(sp1r) == true) {
			} else {
				sp1.setXpos(sp1.getXpos() - (1 * delta));
				sp1r.setX((float) sp1.getXpos());
				if (sp1.getXpos() < 0) {
					sp1.setXpos(0);
					sp1r.setX(0);
				}
			}

			r.setX(r.getX() - (1 * delta));
		}

		if (input.isKeyDown(Input.KEY_S) || input.isKeyDown(Input.KEY_DOWN)) {
			r.setY(r.getY() - (1 * delta));

			if (r.intersects(sp1r) == true) {
			} else {
				sp1.setYpos(sp1.getYpos() + (1 * delta));
				sp1r.setY((float) sp1.getYpos());
				if ((sp1.getYpos() + spielergroesseY) > fensterY) {
					sp1.setYpos(fensterY - spielergroesseY);
					sp1r.setY(fensterY - spielergroesseY);
				}
			}

			r.setY(r.getY() + (1 * delta));
		}

		if (input.isKeyDown(Input.KEY_D) || input.isKeyDown(Input.KEY_RIGHT)) {
			r.setX(r.getX() - (1 * delta));

			if (r.intersects(sp1r) == true || r.intersects(sp1r)) {
			} else {
				sp1.setXpos(sp1.getXpos() + (1 * delta));
				sp1r.setX((float) sp1.getXpos());
				if ((sp1.getXpos() + (spielergroesseX)) > fensterX) {
					sp1.setXpos(fensterX - spielergroesseX);
					sp1r.setX(fensterX - spielergroesseX);
				}
			}
			r.setX(r.getX() + (1 * delta));
		}
//
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		// Hier muss die Karte gezeichnet werden
		zk.render(gc, g);
		g.draw(r);
		// g.drawRect((float) sp1.getXpos(), (float) sp1.getYpos(), 10, 10);
		sp1.getAnimation().draw((float) sp1.getXpos(), (float) sp1.getYpos(), spielergroesseX, spielergroesseY);
		g.draw(sp1r);
	}

	public static void main(String[] args) {
		try {
			AppGameContainer appgc;
			appgc = new AppGameContainer(new DarkChambers("Dark Chambers"));
			appgc.setShowFPS(true);
			appgc.setDisplayMode(fensterX, fensterY, false);
			appgc.start();
		} catch (SlickException ex) {
			Logger.getLogger(DarkChambers.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}

package gui;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import logik.DarkChambers;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 13.12.2016
  * hester 
  */

public class Spielstand extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JLabel jLabel1 = new JLabel();
    private JButton jButton1 = new JButton();
    private JButton jButton2 = new JButton();
    private JButton jButton3 = new JButton();
    private JList jList1 = new JList();
  // Ende Attribute
  
  public Spielstand(int benutzer) { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 400; 
    int frameHeight = 400;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Spielstand");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 385, 361);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    jLabel1.setBounds(0, 0, 110, 20);
    jLabel1.setText("Spielst�nde");
    jPanel1.add(jLabel1);
    jButton1.setBounds(8, 328, 75, 25);
    jButton1.setText("Scores");
    jButton1.setMargin(new Insets(2, 2, 2, 2));
    jButton1.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        jButton1_ActionPerformed(evt, benutzer);
      }
    });
    jPanel1.add(jButton1);
    jButton2.setBounds(152, 328, 75, 25);
    jButton2.setText("Spielen");
    jButton2.setMargin(new Insets(2, 2, 2, 2));
    jButton2.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        jButton2_ActionPerformed(evt);
        
      }
    });
    jPanel1.add(jButton2);
    jButton3.setBounds(296, 328, 75, 25);
    jButton3.setText("L�schen");
    jButton3.setMargin(new Insets(2, 2, 2, 2));
    jButton3.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        jButton3_ActionPerformed(evt);
      }
    });
    jPanel1.add(jButton3);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Spielstand
  
  // Anfang Methoden
  
//  public static void main(String[] args) {
//    new Spielstand(benutzer);
//  } // end of main
  
  public void jButton1_ActionPerformed(ActionEvent evt, int benutzer) {
    Scoreboard sc = new Scoreboard(benutzer);
  } // end of jButton1_ActionPerformed

  public void jButton2_ActionPerformed(ActionEvent evt) {
    DarkChambers.main(null);
  } // end of jButton2_ActionPerformed

  public void jButton3_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
  } // end of jButton3_ActionPerformed

  // Ende Methoden
} // end of class Spielstand
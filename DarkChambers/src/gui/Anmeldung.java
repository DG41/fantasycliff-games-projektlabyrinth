package gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.TextField;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

import sql.DatenbankMain;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList; 

public class Anmeldung {

	public static void main(String[] args) {
		//erstellen JFrame
		JFrame frame=new JFrame(); 
		frame.setSize(400, 400);
		
		
		//Borderlayout
		int vgap=5;
		int hgap=5;
		BorderLayout layout = new BorderLayout(hgap,vgap);
		
		//JPanel
		JPanel panel=new JPanel();
		panel.setLayout(layout);
		panel.add(new JLabel("Anmeldung"),BorderLayout.NORTH);
		frame.add(panel);
		
		//Array f�r Namen
		ArrayList<String> liste01 = DatenbankMain.benutzerListeAnzeigen();
		String [] data=new String [liste01.size()];
		for (int i = 0; i < liste01.size(); i++) {
			data[i] = liste01.get(i);
		}
		
		
		//Liste d. Namen ausgeben
		JList list = new JList(data); //data has type Object[]
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);
		panel.add(list,BorderLayout.CENTER);

		//Neuen Namen hinzuf�gen
		GridLayout grid=new GridLayout(2,2,4,5);
		JPanel south=new JPanel();
		panel.add(south, BorderLayout.SOUTH);
		south.setLayout(grid);
		south.add(new JLabel("Neuen Namen eingeben: "));
		TextField text=new TextField();
		south.add(text);
		//Weiterleitung
		
		KeyListener listen=new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				int benutzer;
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
				//neue GUI starten
					benutzer = DatenbankMain.benutzerIDAnzeigen((String) list.getSelectedValue());
				Spielstand st = new Spielstand(benutzer);
				//Fenster verschwinden lassen
				frame.dispose();}
			}
		};
		list.addKeyListener(listen);
		text.addKeyListener(listen);
	   
		frame.setVisible(true);
	
 }
}
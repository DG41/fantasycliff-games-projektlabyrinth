package gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.TextField;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import sql.DatenbankMain;

public class Scoreboard {
	public Scoreboard (int benutzer){
	//erstellen JFrame
	JFrame frame=new JFrame(); 
	frame.setSize(400, 400);
	
	
	//Borderlayout
	int vgap=5;
	int hgap=5;
	BorderLayout layout = new BorderLayout(hgap,vgap);
	
	//JPanel
	JPanel panel=new JPanel();
	panel.setLayout(layout);
	panel.add(new JLabel("Scoreboard"),BorderLayout.NORTH);
	frame.add(panel);
	
	//Array f�r Namen
	ArrayList<String> liste01 = DatenbankMain.scoreboardAnzeigen(benutzer);
	String [] data=new String [liste01.size()];
	for (int i = 0; i < liste01.size(); i++) {
		data[i] = liste01.get(i);
	}
	
	//Liste d. Namen ausgeben
	JList list = new JList(data); //data has type Object[]
	list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	list.setLayoutOrientation(JList.VERTICAL);
	list.setVisibleRowCount(-1);
	panel.add(list,BorderLayout.CENTER);

	
   
	frame.setVisible(true);	
	}
	public static void main (String [] args){
	
}
}
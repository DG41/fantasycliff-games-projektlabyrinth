package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DatenbankMain {
	private static Connection con = null;
	private static String dbHost = "localhost"; // Hostname
	private static String dbPort = "3306"; // Port -- Standard: 3306
	private static String dbName = "dark_chambers"; // Datenbankname
	private static String dbUser = "root"; // Datenbankuser
	private static String dbPass = ""; // Datenbankpasswort

	private DatenbankMain() {
		try {
			Class.forName("com.mysql.jdbc.Driver"); // Datenbanktreiber f�r JDBC
													// Schnittstellen laden.

			// Verbindung zur JDBC-Datenbank herstellen.
			con = DriverManager.getConnection("jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName + "?" + "user="
					+ dbUser + "&" + "password=" + dbPass);
		} catch (ClassNotFoundException e) {
			System.out.println("Treiber nicht gefunden");
		} catch (SQLException e) {
			System.out.println("Verbindung nicht moglich");
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}
	}

	private static Connection getInstance() {
		if (con == null)
			new DatenbankMain();
		return con;
	}

	// Gebe das Scoreboard als Liste aus
	public static ArrayList<String> scoreboardAnzeigen(int benutzer) {
		con = getInstance();

		ArrayList<String> scoreboard = new ArrayList<String>();

		String a;
		int b;
		benutzer = 0;

		if (con != null) {
			// Abfrage-Statement erzeugen.
			Statement query;
			try {
				query = con.createStatement();

				// Tabelle anzeigen
				String sql = "SELECT `score`,`charactername` From `t_speicherstaende` WHERE `f_benutzer_id` = '" + benutzer + "' ORDER BY `score` DESC;";
				ResultSet result = query.executeQuery(sql);

				while (result.next()) {
					a = result.getString("charactername");
					b = result.getInt("score");

					scoreboard.add(a + " , " + b); // " , " zum trennen von
													// Charname und Score
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return scoreboard;
	}

	// Gebe Benutzer als Liste aus
	public static ArrayList<String> benutzerListeAnzeigen() {
		con = getInstance();

		ArrayList<String> liste01 = new ArrayList<String>();

		String a;

		if (con != null) {
			// Abfrage-Statement erzeugen.
			Statement query;
			try {
				query = con.createStatement();

				// Tabelle anzeigen
				String sql = "SELECT `benutzername` From `t_benutzer`;";
				ResultSet result = query.executeQuery(sql);

				while (result.next()) {
					a = result.getString("benutzername");

					liste01.add(a); 
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return liste01;
	}
	
	// Gebe Benutzer als Liste aus
		public static int benutzerIDAnzeigen(String benutzer) {
			con = getInstance();

			int liste03 = 0;


			if (con != null) {
				// Abfrage-Statement erzeugen.
				Statement query;
				try {
					query = con.createStatement();

					// Tabelle anzeigen
					String sql = "SELECT `p_benutzer_id` From `t_benutzer` WHERE `benutzername` = '" + benutzer + "';";
					ResultSet result = query.executeQuery(sql);
					while (result.next()) {
						liste03 = result.getInt("p_benutzer_id");
					}
					

				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return liste03;
		}

	// Gebe speicherst�nde als Liste aus
	public static ArrayList<String> speicherstaendeAnzeigen(String benutzer) {
		con = getInstance();

		ArrayList<String> liste02 = new ArrayList<String>();

		String a;

		if (con != null) {
			// Abfrage-Statement erzeugen.
			Statement query;
			try {
				query = con.createStatement();

				// Tabelle anzeigen
				String sql = "SELECT `charactername`, `score` From `t_speicherstaende` WHERE `t_benutzer` = '" + benutzer
						+ "';";
				ResultSet result = query.executeQuery(sql);

				while (result.next()) {
					a = result.getString("charactername");

					liste02.add(a); // " , " zum trennen von
									// Charname und Score
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return liste02;
	}

	public static void benutzerHinzufuegen(String benutzer) {
		con = getInstance();

		if (con != null) {
			// Abfrage-Statement erzeugen.
			Statement query;
			try {
				query = con.createStatement();

				// Tabelle anzeigen
				String sql = "INSERT INTO `t_benutzer`(`benutzername`) VALUES ('" + benutzer + "');";

				ResultSet result = query.executeQuery(sql);

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

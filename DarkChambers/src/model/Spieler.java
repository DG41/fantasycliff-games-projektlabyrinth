package model;

import org.newdawn.slick.Animation;

/**
 * @author Dican
 *
 */
public class Spieler extends Entity {
	private int ammo_pistol;
	private int ammo_shotgun;
	private boolean axt;

	public Spieler(double xpos, double ypos, int hp, int speed, int close_damage, boolean axt, int ammo_p, int ammo_s,
			Animation anim) {
		super();
		this.setXpos(xpos);
		this.setYpos(ypos);
		this.setHp(hp);
		this.setSpeed(speed);
		this.setClose_damage(close_damage);
		
		this.setAnimation(anim);

		this.ammo_pistol = ammo_p;
		this.ammo_shotgun = ammo_s;
		this.setAxt(axt);
	}

	/**
	 * @return the ammo_pistol
	 */
	public int getAmmo_pistol() {
		return ammo_pistol;
	}

	/**
	 * @param ammo_pistol
	 *            the ammo_pistol to set
	 */
	public void setAmmo_pistol(int ammo_pistol) {
		this.ammo_pistol = ammo_pistol;
	}

	/**
	 * @return the ammo_shotgun
	 */
	public int getAmmo_shotgun() {
		return ammo_shotgun;
	}

	/**
	 * @param ammo_shotgun
	 *            the ammo_shotgun to set
	 */
	public void setAmmo_shotgun(int ammo_shotgun) {
		this.ammo_shotgun = ammo_shotgun;
	}

	/**
	 * @return the axt
	 */
	public boolean isAxt() {
		return axt;
	}

	/**
	 * @param axt
	 *            the axt to set
	 */
	public void setAxt(boolean axt) {
		this.axt = axt;
	}

}

package model;

import java.util.ArrayList;

import org.newdawn.slick.Image;

import logik.DarkChambers;

public class Karte {

	private DarkChambers logik;
	private int breite;
	private int hoehe;
	private final Image hintergrund;

	private ArrayList<Mauer> mauerteile;
	private ArrayList<Gegner> gegnerliste;
	
	public Karte(Image hintergrund){
		this.hintergrund = hintergrund;
		this.breite = hintergrund.getWidth();
		this.hoehe = hintergrund.getHeight();
		
		mauerteile = new ArrayList<Mauer>();
		gegnerliste = new ArrayList<Gegner>();
		
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}

	public ArrayList<Mauer> getMauerteile() {
		return mauerteile;
	}

	public void setMauerteile(ArrayList<Mauer> mauerteile) {
		this.mauerteile = mauerteile;
	}

	public ArrayList<Gegner> getGegnerliste() {
		return gegnerliste;
	}

	public void setGegnerliste(ArrayList<Gegner> gegnerliste) {
		this.gegnerliste = gegnerliste;
	}

	public Image getHintergrund() {
		return hintergrund;
	}
	
	
}

package model;

import org.newdawn.slick.Animation;

/**
 * @author Dican
 *
 */
public abstract class Entity {
	private int hp;
	private int speed;
	private int close_damage;	
	private double xpos;
	private double ypos;
	private Animation animation;

	/**
	 * @return the hp
	 */
	public int getHp() {
		return hp;
	}

	/**
	 * @param hp
	 *            the hp to set
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}

	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @param speed
	 *            the speed to set
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	/**
	 * @return the close_damage
	 */
	public int getClose_damage() {
		return close_damage;
	}

	/**
	 * @param close_damage
	 *            the close_damage to set
	 */
	public void setClose_damage(int close_damage) {
		this.close_damage = close_damage;
	}

	/**
	 * @return the xpos
	 */
	public double getXpos() {
		return (int) Math.round(xpos);
	}

	/**
	 * @param xpos
	 *            the xpos to set
	 */
	public void setXpos(double xpos) {
		this.xpos = xpos;
	}

	/**
	 * @return the ypos
	 */
	public double getYpos() {
		return (int) Math.round(ypos);
	}

	/**
	 * @param ypos
	 *            the ypos to set
	 */
	public void setYpos(double ypos) {
		this.ypos = ypos;
	}

	/**
	 * @return the animation
	 */
	public Animation getAnimation() {
		return animation;
	}

	/**
	 * @param animation the animation to set
	 */
	public void setAnimation(Animation animation) {
		this.animation = animation;
	}

}

CREATE DATABASE Dark_Chambers;

CREATE TABLE `t_benutzer` (
  `p_benutzer_id` int(11) NOT NULL AUTO_INCREMENT,
  `benutzername` varchar(20) NOT NULL,
  PRIMARY KEY (`p_benutzer_id`)
);

CREATE TABLE `t_speicherstaende` (
  `p_speicher_id` INT NOT NULL AUTO_INCREMENT,
  `charactername` VARCHAR(20) NOT NULL,
  `f_benutzer_id` INT NOT NULL,
  `score` INT NOT NULL,
  PRIMARY KEY (`p_speicher_id`),
  CONSTRAINT `fk_benutzer_id`
    FOREIGN KEY (`f_benutzer_id`)
    REFERENCES `dark_chambers`.`t_benutzer` (`p_benutzer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

